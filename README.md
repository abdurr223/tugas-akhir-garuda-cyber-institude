# Tugas Akhir

## Garuda Cyber Institute

#### Kelompok 3

    Nama : Abdurrahman

[Link Tugas](http://lilinkecil.xyz)

=============================================================================================

# Laravel 9 E-Commerce

Repositori ini untuk yang masih dalam belajar tingkat awal menggunakan Laravel V.9

## Installation

Pastikan anda sudah masuk ke direktori installasi, setelah itu ketikkan perintah berikut :

    composer install

Copy file .env.example dan ubah rename ke .env agar bisa digunakan untuk mengkonfigurasi beberapa hal. gunakan perintah dibawah :

    cp .env.example .env

Setelah .env dibuat ketikkan perintah dibawah berikut agar Key yang kosong digenerate secara otomatis :

    php artisan key:generate

Lalu buat database baru, sesuaikan nama databasenya dengan yang ada di file .env

Jalankan migrasi db dan seeding :

    php artisan migrate --seed

Jalankan Perintah dibawah agar project bisa diakses secara local

    php artisan serve

=============================================================================================

# Project ini menggunakan

Migration

Model + Eloquent

Controller

Laravel Auth + Middleware

View (Blade)

CRUD

Laravel + Library

Menggunakan Template AdminLTE
