@extends('layout.master') @push('css')
<link
    href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
    rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
    crossorigin="anonymous"
/>
<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css"
    integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g=="
    crossorigin="anonymous"
    referrerpolicy="no-referrer"
/>
@endpush @section('content')
<h1 class="text-center mb-4 mt-2">Tambah Data Pegawai</h1>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <form
                        action="/tambahdata"
                        method="POST"
                        enctype="multipart/form-data"
                    >
                        @csrf
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label"
                                >Nama Lengkap</label
                            >
                            <input
                                type="text"
                                name="nama"
                                class="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                            />
                            @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail11" class="form-label"
                                >Jenis Kelamin</label
                            >
                            <select
                                class="form-select"
                                name="jeniskelamin"
                                aria-label="Default select example"
                            >
                                <option selected>Pilih Jenis Kelamin</option>
                                <option value="Laki - Laki">Laki - Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail111" class="form-label"
                                >No Telpon</label
                            >
                            <input
                                type="number"
                                name="notelpon"
                                class="form-control"
                                id="exampleInputEmail111"
                                aria-describedby="emailHelp"
                            />
                            @error('notelpon')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label
                                for="exampleInputEmail1111"
                                class="form-label"
                                >Masukkan Foto</label
                            >
                            <input
                                type="file"
                                name="foto"
                                class="form-control"
                                id="exampleInputEmail1111"
                                aria-describedby="emailHelp"
                            />
                        </div>

                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
