@extends('layout.master')
@section('content')
<h1 class="text-center mb-4 mt-2">Data Pegawai</h1>
        <div class="container">
            <a href="/tambahpegawai" class="btn btn-success">Tambah Data +</a>
            <div class="row">
                @if($message = Session::get('success'))
                <div class="alert alert-success" role="alert">{{$message}}</div>
                @endif
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Jenis Kelamin</th>
                            <th scope="col">No telepon</th>
                            <th scope="col">Dibuat</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                        <tr>
                            <th scope="row">{{$loop -> iteration}}</th>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->jeniskelamin}}</td>
                            <td>{{$item->notelpon}}</td>
                            <td>{{$item->created_at->diffforhumans()}}</td>
                            <td>
                                <button type="button" class="btn btn-primary">
                                    Edit
                                </button>
                                <a href="/tampilkandata/{{$row->id}}" class="btn btn-info">Edit</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection