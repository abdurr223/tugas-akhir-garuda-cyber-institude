<?php

use App\Models\Employee;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $jumlahpegawai = Employee::count();
    return view('welcome', compact('jumlahpegawai'));
})->middleware('auth');

Route::get('/pegawai', [EmployeeController::class,'index'])->name('pegawai')->middleware('auth');

Route::get('/tambahpegawai', [EmployeeController::class,'tambahpegawai'])->name('tambahpegawai');
Route::post('/tambahdata', [EmployeeController::class,'tambahdata'])->name('tambahdata');
Route::get('/tampilkandata/{id}', [EmployeeController::class,'tampilkandata'])->name('tampilkandata');
Route::post('/perbaharuidata/{id}', [EmployeeController::class,'perbaharuidata'])->name('perbaharuidata');
Route::get('/hapus/{id}', [EmployeeController::class,'hapus'])->name('hapus');

// exportpdf
Route::get('/exportpdf', [EmployeeController::class,'exportpdf'])->name('exportpdf');

Route::get('/login', [LoginController::class,'login'])->name('login');
Route::post('/loginproses', [LoginController::class,'loginproses'])->name('loginproses');

Route::get('/register', [LoginController::class,'register'])->name('register');
Route::post('/registeruser', [LoginController::class,'registeruser'])->name('registeruser');

Route::get('/logout', [LoginController::class,'logout'])->name('logout');

